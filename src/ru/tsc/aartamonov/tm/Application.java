package ru.tsc.aartamonov.tm;

import ru.tsc.aartamonov.tm.constant.TerminalConst;

import static ru.tsc.aartamonov.tm.constant.TerminalConst.CMD_VERSION;

import static ru.tsc.aartamonov.tm.constant.TerminalConst.CMD_HELP;

import static ru.tsc.aartamonov.tm.constant.TerminalConst.CMD_ABOUT;

public class Application {

    public static void main(String[] args) {
        showWelcome();
        run(args);
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.2.0.0");
        System.exit(0);
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name:    Anatoly Artamonov");
        System.out.println("E-mail:  aartamonov@t1-consulting.ru");
        System.exit(0);
    }

    private static void showHelp(){
        System.out.println("[HELP]");
        System.out.printf("%s - Display program version.\n", CMD_VERSION);
        System.out.printf("%s - Display developer info.\n", CMD_ABOUT);
        System.out.printf("%s - Display list of terminal commands.\n", CMD_HELP);
        System.exit(0);
    }

    private static void showWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void showErrorArgument(){
        System.err.println("Error! This argument not supported...");
        System.exit(0);
    }

    private static void run(final String[] args) {
        if (args == null || args.length < 1) {
            showErrorArgument();
        }
        final String arg = args[0];
        switch (arg) {
            case CMD_HELP:
                showHelp();
                break;
            case CMD_VERSION:
                showVersion();
                break;
            case CMD_ABOUT:
                showAbout();
                break;
            default:
                showErrorArgument();
                break;
        }
    }

}
