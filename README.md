# TASK-MANAGER

## DEVELOPER

NAME: Anatoly Artamonov

E-MAIL: artamonov.av@hotmail.com

## SOFTWARE

**JAVA**: JDK 1.8

**OS**: 
* Windows 10 LTSC version 1809;  
* Ubuntu 20.04.3 LTS (Focal Fossa).

## HARDWARE

**CPU**: i5

**RAM**: 16 Gb

**SSD**: 256 Gb

## APPLICATION RUN

* Windows

```powershell
cd out\artifacts\task_manager
java -jar task-manager.jar
```

* Ubuntu

```shell
cd out/artifacts/task_manager/
java -jar task-manager.jar
```
